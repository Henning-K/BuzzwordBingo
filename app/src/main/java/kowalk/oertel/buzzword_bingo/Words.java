package kowalk.oertel.buzzword_bingo;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "words")
class Words {
    @DatabaseField(generatedId = true, columnName = "_id")
    private int id;
    @DatabaseField(foreign = true, canBeNull = false)
    private WordLists wordlist;
    @DatabaseField(canBeNull = false)
    private String name;

    Words() {
    }

    Words(String name) {
        this.name = name;
    }

    Words(String name, WordLists wordlist) {
        this.name = name;
        this.wordlist = wordlist;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public WordLists getWordlist() {
        return wordlist;
    }

    void setWordlist(WordLists wordlist) {
        this.wordlist = wordlist;
    }

    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
