package kowalk.oertel.buzzword_bingo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends OrmLiteBaseActivity<BingoDbHelper> {
    private Spinner wordlists_spinner;

    private WordLists current_wordlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wordlists_spinner = findViewById(R.id.main_spinner_choose_list);
        loadWordLists();

        Button main_btn_manage_wordlists = findViewById(R.id.main_btn_manage_lists);
        Button main_btn_start_game = findViewById(R.id.main_btn_start_game);

        main_btn_manage_wordlists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageWordlistsActivity.call_me(MainActivity.this);
            }
        });

        main_btn_start_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_wordlist = (WordLists) (wordlists_spinner.getSelectedItem());

                if (getHelper().get_word_count_for_list(current_wordlist) < 24) {
                    Toast.makeText(MainActivity.this, "Weniger als 24 Elemente in der gewählten Liste. Spiel nicht möglich.", Toast.LENGTH_LONG).show();
                    return;
                }



                int selected_row_id = current_wordlist.getId();
                GameActivity.call_me(MainActivity.this, selected_row_id);
            }
        });
    }

    private void loadWordLists() {
        try {
            Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
            List<WordLists> wordLists = new ArrayList<>(wordListsDao.queryForAll());
            ArrayAdapter<WordLists> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, wordLists);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            wordlists_spinner.setAdapter(adapter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // Credit for this "call_me" pattern goes to:
    // https://github.com/j256/ormlite-examples/blob/master/android/ClickCounter/src/com/kagii/clickcounter/CreateCounter.java#L93
    // and subsequently integrated into all my classes
    static void call_me(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }
}
