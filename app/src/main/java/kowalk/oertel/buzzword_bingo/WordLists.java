package kowalk.oertel.buzzword_bingo;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "wordLists")
public class WordLists {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(unique = true, canBeNull = false)
    private String title;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<Words> words;

    WordLists() {
    }

    WordLists(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ForeignCollection<Words> getWords() {
        return words;
    }

    public void setWords(ForeignCollection<Words> words) {
        this.words = words;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
