package kowalk.oertel.buzzword_bingo;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.android.apptools.OrmLiteCursorAdapter;
import com.j256.ormlite.android.apptools.OrmLiteCursorLoader;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;

import java.sql.SQLException;

public class EditWordsActivity extends OrmLiteBaseActivity<BingoDbHelper> implements LoaderManager.LoaderCallbacks<Cursor> {

    private final String LOG_NAME = EditWordsActivity.class.toString();
    private EditWordsAdapter adapter;
    private Dao<Words, Integer> wordsDao;
    private PreparedQuery<Words> preparedQuery;

    public static final int FROM_NEW = 0;
    public static final int FROM_EDIT_WL = 1;
    public static final String INTENT_FROM_STRING = "FROM";
    public static final String INTENT_ID_STRING = "wordlist_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_words);

        final int wordlist_id = getIntent().getIntExtra(INTENT_ID_STRING, 0);

        ListView words_listview = findViewById(R.id.lv_words);
        Button btn_back = findViewById(R.id.edit_words_btn_back);

        try {
            wordsDao = getHelper().getWordsDao();
            // only query for the rows that have belong to the wordlist which this activity was called on.
            preparedQuery = wordsDao.queryBuilder().where().eq("wordlist_id", wordlist_id).prepare();
        } catch (SQLException e) {
            Log.e(LOG_NAME, "Error encountered while loading words", e);
        }

        adapter = new EditWordsAdapter(this);
        adapter.setPreparedQuery(preparedQuery);
        words_listview.setAdapter(adapter);

        getLoaderManager().initLoader(0, null, this);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int from_which = getIntent().getIntExtra(INTENT_FROM_STRING, FROM_EDIT_WL);
                Intent intent = new Intent(EditWordsActivity.this,
                        from_which == FROM_NEW ? NewWordlistActivity.class
                                : EditWordlistActivity.class);
                intent.putExtra(INTENT_ID_STRING, wordlist_id);
                startActivity(intent);
            }
        });
    }

    static void call_me(Context context, int wordlist_id, int from_where) {
        Intent intent = new Intent(context, EditWordsActivity.class);
        intent.putExtra(EditWordsActivity.INTENT_ID_STRING, wordlist_id);
        intent.putExtra(EditWordsActivity.INTENT_FROM_STRING, from_where);
        context.startActivity(intent);
    }

    // LoaderManager callbacks code taken from
    // https://github.com/amityadav1984/ORMLite-Tutorial/blob/master/src/ormlite/app/MainActivity.java#L71
    // and modified
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new OrmLiteCursorLoader<Words>(this, wordsDao, preparedQuery);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        CursorAdapter _adapter = adapter;
        if (_adapter != null && cursor != null) {
            _adapter.swapCursor(cursor);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        CursorAdapter _adapter = adapter;
        if (_adapter != null) {
            _adapter.swapCursor(null);
        }
    }


    private class EditWordsAdapter extends OrmLiteCursorAdapter<Words, View> {

        private final LayoutInflater inflater;

        EditWordsAdapter(Context context) {
            super(context);
            inflater = LayoutInflater.from(context);
        }

        @Override
        public void bindView(View view, Context context, Words words) {
            ViewHolder holder = (ViewHolder) view.getTag();
            holder.word_id = words.getId();
            holder.edit_words_item_tv_word.setText(words.getName());
        }

        @Override
        public View newView(final Context context, final Cursor cursor, ViewGroup parent) {
            View view = inflater.inflate(R.layout.item_word, null);

            final ViewHolder holder = new ViewHolder();
            holder.edit_words_item_tv_word = view.findViewById(R.id.item_word_tv_word_name);
            holder.edit_words_item_btn_edit = view.findViewById(R.id.item_word_btn_edit);
            holder.edit_words_item_btn_delete = view.findViewById(R.id.item_word_btn_delete);


            holder.edit_words_item_btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(LOG_NAME, "clicked delete button on " + holder.edit_words_item_tv_word.getText().toString());
//                    Dao<Words, Integer> edit_words_dao;
                    try {
//                        edit_words_dao = getHelper().getWordsDao();
                        wordsDao.deleteById(holder.word_id);
                    } catch (SQLException e) {
                        Log.e(LOG_NAME, "Error encountered while loading words", e);
                    }
                }
            });

            holder.edit_words_item_btn_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i(LOG_NAME, "clicked edit button on " + holder.edit_words_item_tv_word.getText().toString());
                    final AlertDialog.Builder edit_word_dialog = new AlertDialog.Builder(context);
//                    LayoutInflater dialog_inflater = LayoutInflater.from(edit_word_dialog.getContext());
                    View edit_view = getLayoutInflater().inflate(R.layout.dialog_edit_word_name, null);
                    final EditText et_change_name_input = edit_view.findViewById(R.id.dialog_edit_word_name_et_input);
                    et_change_name_input.setText(holder.edit_words_item_tv_word.getText());

                    edit_word_dialog.setTitle("Wort/Phrase ändern")
                            .setPositiveButton(R.string.btn_ok_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // change the "Words" item's text
                                    String changed_word = et_change_name_input.getText().toString();
                                    if (changed_word.equals(holder.edit_words_item_tv_word.getText().toString()) || changed_word.isEmpty()) {
                                        return;
                                    }
                                    try {
                                        Words word_to_change = wordsDao.queryForId(holder.word_id);
                                        word_to_change.setName(changed_word);
                                        wordsDao.update(word_to_change);
                                    } catch (SQLException e) {
                                        Log.e(LOG_NAME, "Error encountered while updating word", e);
                                    }
                                }
                            })
                            .setNegativeButton(R.string.btn_cancel_text, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // don't do anything
                                }
                            })
                            .setView(edit_view)
                            .create()
                            .show();
                }
            });

            view.setTag(holder);
            return view;
        }

        private class ViewHolder {
            int word_id;
            TextView edit_words_item_tv_word;
            ImageButton edit_words_item_btn_edit;
            ImageButton edit_words_item_btn_delete;
        }
    }
}
