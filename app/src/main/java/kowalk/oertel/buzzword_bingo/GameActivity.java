package kowalk.oertel.buzzword_bingo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GameActivity extends OrmLiteBaseActivity<BingoDbHelper> {

    private static final String ID_STRING = "wordlist_id";
    private static final String LOG_NAME = GameActivity.class.toString();

    private WordLists current_wordlist;
    private List<Words> selected_words;
    private ArrayList<Button> buttons;
    private boolean game_won = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        int current_wordlist_id = getIntent().getIntExtra(ID_STRING, 0);

        try {
            Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
            current_wordlist = wordListsDao.queryForId(current_wordlist_id);

            Dao<Words, Integer> wordsDao = getHelper().getWordsDao();
            ArrayList<Words> gotten_words = new ArrayList<>(wordsDao.queryBuilder().where().eq(ID_STRING, current_wordlist).query());
            Collections.shuffle(gotten_words);
            selected_words = gotten_words.subList(0, 24);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        Button[] all_clickable_buttons = {
                findViewById(R.id.game_btn00),
                findViewById(R.id.game_btn01),
                findViewById(R.id.game_btn02),
                findViewById(R.id.game_btn03),
                findViewById(R.id.game_btn04),
                findViewById(R.id.game_btn10),
                findViewById(R.id.game_btn11),
                findViewById(R.id.game_btn12),
                findViewById(R.id.game_btn13),
                findViewById(R.id.game_btn14),
                findViewById(R.id.game_btn20),
                findViewById(R.id.game_btn21),
                findViewById(R.id.game_btn23),
                findViewById(R.id.game_btn24),
                findViewById(R.id.game_btn30),
                findViewById(R.id.game_btn31),
                findViewById(R.id.game_btn32),
                findViewById(R.id.game_btn33),
                findViewById(R.id.game_btn34),
                findViewById(R.id.game_btn40),
                findViewById(R.id.game_btn41),
                findViewById(R.id.game_btn42),
                findViewById(R.id.game_btn43),
                findViewById(R.id.game_btn44)
        };
        buttons = new ArrayList<>(Arrays.asList(all_clickable_buttons));

        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setText(selected_words.get(i).getName());
            ButtonTag tag = new ButtonTag(selected_words.get(i));
            buttons.get(i).setTag(tag);
        }

        Button game_back_button = findViewById(R.id.game_btn_back);

        game_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.call_me(GameActivity.this);
            }
        });

        Button game_title_button = findViewById(R.id.game_btn_title);

        ButtonTag title_tag = new ButtonTag(null);
        title_tag.set_is_ticked(true);

        game_title_button.setText(current_wordlist.getTitle());
        game_title_button.setTag(title_tag);
    }

    static void call_me(Context context, int wordlist_id) {
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra(ID_STRING, wordlist_id);
        context.startActivity(intent);
    }

    // todo game layout and logic
    public void set_bingo_mark(View view) {
        if (game_won) {
            return;
        }
        int element_id = view.getId();
        ButtonTag element_tag = (ButtonTag) view.getTag();
        element_tag.toggle_tick();
        Log.i(LOG_NAME, "tapped " + element_tag.get_words().getName());
        view.setBackgroundColor(getResources().getColor(element_tag.get_is_ticked() ? android.R.color.holo_red_light : android.R.color.holo_green_light, null));

        if (get_row0() || get_row1() || get_row2() || get_row3() || get_row4() || get_col0() || get_col1() || get_col2() || get_col3() || get_col4() || get_diagonalNW2SE() || get_diagonalSW2NE()) {
            game_won = true;
            Toast.makeText(GameActivity.this, "BINGO!", Toast.LENGTH_LONG).show();
        }
    }

    private boolean check_list(List<Button> button_list) {
        return button_list.stream().map((Button btn) ->
                ((ButtonTag) btn.getTag()).get_is_ticked()
        ).reduce(true, (Boolean a, Boolean b) -> a && b);
    }

    private boolean get_row0() {
        return check_list(buttons.subList(0, 5));
    }

    private boolean get_row1() {
        return check_list(buttons.subList(5, 10));
    }

    private boolean get_row2() {
        return check_list(buttons.subList(10, 14));
    }

    private boolean get_row3() {
        return check_list(buttons.subList(14, 19));
    }

    private boolean get_row4() {
        return check_list(buttons.subList(19, 24));
    }

    private boolean get_col0() {
        return check_list(Arrays.asList(buttons.get(0), buttons.get(5), buttons.get(10), buttons.get(14), buttons.get(19)));
    }

    private boolean get_col1() {
        return check_list(Arrays.asList(buttons.get(1), buttons.get(6), buttons.get(11), buttons.get(15), buttons.get(20)));
    }

    private boolean get_col2() {
        return check_list(Arrays.asList(buttons.get(2), buttons.get(7), buttons.get(16), buttons.get(21)));
    }

    private boolean get_col3() {
        return check_list(Arrays.asList(buttons.get(3), buttons.get(8), buttons.get(12), buttons.get(17), buttons.get(22)));
    }

    private boolean get_col4() {
        return check_list(Arrays.asList(buttons.get(4), buttons.get(9), buttons.get(13), buttons.get(18), buttons.get(23)));
    }

    private boolean get_diagonalNW2SE() {
        return check_list(Arrays.asList(buttons.get(0), buttons.get(6), buttons.get(17), buttons.get(23)));
    }

    private boolean get_diagonalSW2NE() {
        return check_list(Arrays.asList(buttons.get(19), buttons.get(15), buttons.get(8), buttons.get(4)));
    }

    private class ButtonTag {
        boolean is_ticked;
        Words words;

        ButtonTag(Words words) {
            is_ticked = false;
            this.words = words;
        }

        void toggle_tick() {
            this.is_ticked = !this.is_ticked;
        }

        boolean get_is_ticked() {
            return this.is_ticked;
        }

        Words get_words() {
            return this.words;
        }

        void set_is_ticked(boolean is_ticked) {
            this.is_ticked = is_ticked;
        }

        void set_words(Words words) {
            this.words = words;
        }
    }
}
