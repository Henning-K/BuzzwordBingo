package kowalk.oertel.buzzword_bingo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

class BingoDbHelper extends OrmLiteSqliteOpenHelper {

    private static final String LOG = BingoDbHelper.class.getName();
    private static final String DB_NAME = "wordlists.db";
    private static final int DB_VERSION = 4;

    private Dao<WordLists, Integer> wordListsDao;
    private Dao<Words, Integer> wordsDao;

    BingoDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, WordLists.class);
            TableUtils.createTableIfNotExists(connectionSource, Words.class);
        } catch (SQLException ex) {
            Log.e(LOG, "error creating table", ex);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int old_version, int new_version) {
        try {
            TableUtils.dropTable(connectionSource, Words.class, true);
            TableUtils.dropTable(connectionSource, WordLists.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e(LOG, "Could not upgrade the tables from " + old_version + " to " + new_version, e);
        }
    }

    Dao<WordLists, Integer> getWordListsDao() throws SQLException {
        if (wordListsDao == null) {
            wordListsDao = getDao(WordLists.class);
        }
        return wordListsDao;
    }

    Dao<Words, Integer> getWordsDao() throws SQLException {
        if (wordsDao == null) {
            wordsDao = getDao(Words.class);
        }
        return wordsDao;
    }

    void delete_wordlist(WordLists wordLists) {
        if (wordLists != null) {
            try {
                Dao<WordLists, Integer> wordListsDao = getWordListsDao();
                Dao<Words, Integer> wordsDao = getWordsDao();

                // Delete all foreign elements associated with this list first, then delete
                // the list row itself, since ORMLite does not support cascading deletes.
                DeleteBuilder db = wordsDao.deleteBuilder();
                db.where().eq("wordlist_id", wordLists.getId());
                //noinspection unchecked
                wordsDao.delete(db.prepare());
                wordListsDao.delete(wordLists);
            } catch (SQLException e) {
                Log.e(LOG, "deleting wordlist " + wordLists.getTitle() + " failed: ", e);
            }
        }
    }

    long get_word_count_for_list(WordLists wordlists) {
        long res = 0;
        if (wordlists != null) {
            try {
                Dao<Words, Integer> wordsDao = getWordsDao();
                res = wordsDao.queryBuilder().where().eq("wordlist_id", wordlists).countOf();
            } catch (SQLException e) {
                Log.e(LOG, "getting words count for " + wordlists.getTitle() + " failed: ", e);
            }
        }
        return res;
    }
}
