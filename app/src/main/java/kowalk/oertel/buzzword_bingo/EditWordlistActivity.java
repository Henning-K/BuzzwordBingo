package kowalk.oertel.buzzword_bingo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EditWordlistActivity extends OrmLiteBaseActivity<BingoDbHelper> {
    public static final int FILE_REQ_CODE = 1234567;
    private final String LOG_NAME = EditWordlistActivity.class.getName();
    private WordLists current_wordlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_wordlist);

        int current_wordlist_id = getIntent().getIntExtra("wordlist_id", 0);

        try {
            Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
            current_wordlist = wordListsDao.queryForId(current_wordlist_id);
        } catch (SQLException e) {
            Log.e(LOG_NAME, "failed to get the Wordlist by ID for editing.", e);
        }

        TextView edit_wordlist_tv_current_wordlist = findViewById(R.id.edit_wordlist_tv_wordlist_name);
        final EditText edit_wordlist_et_input_new_word = findViewById(R.id.edit_wordlist_et_input_new_word);
        Button edit_wordlist_btn_add_new_word = findViewById(R.id.edit_wordlist_btn_add_word);
        Button edit_wordlist_btn_edit_words = findViewById(R.id.edit_wordlist_btn_edit_words);
        Button edit_wordlist_btn_import_from_file = findViewById(R.id.edit_wordlist_btn_import_words_from_file);
        Button edit_wordlist_btn_back = findViewById(R.id.edit_wordlist_btn_back);
        Button edit_wordlist_btn_delete = findViewById(R.id.edit_wordlist_btn_delete);
        Button edit_wordlist_btn_confirm = findViewById(R.id.edit_wordlist_btn_ok);

        edit_wordlist_tv_current_wordlist.setText(current_wordlist.getTitle());

        edit_wordlist_btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHelper().delete_wordlist(current_wordlist);
                MainActivity.call_me(EditWordlistActivity.this);
            }
        });

        edit_wordlist_btn_add_new_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = edit_wordlist_et_input_new_word.getText().toString();
                if (input.isEmpty()) {
                    Toast.makeText(EditWordlistActivity.this, "Bitte oben erst ein Wort eingeben.", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        Dao<Words, Integer> wordsDao = getHelper().getWordsDao();
                        Words words = new Words(input, current_wordlist);
                        wordsDao.create(words);
                    } catch (SQLException e) {
                        Log.e(LOG_NAME, "failed to get the Wordlist by ID for editing.", e);
                    }
                }
            }
        });

        edit_wordlist_btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.call_me(EditWordlistActivity.this);
            }
        });

        edit_wordlist_btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // In case a wordlist was created from this activity, update the persisted version just in case
                if (current_wordlist != null) {
                    try {
                        Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
                        // just to be sure ALL alterations are persisted.
                        wordListsDao.update(current_wordlist);
                    } catch (SQLException e) {
                        Log.e(LOG_NAME, "ok_button on_click closure: ", e);
                    }
                }

                MainActivity.call_me(EditWordlistActivity.this);
            }
        });

        edit_wordlist_btn_edit_words.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditWordsActivity.call_me(EditWordlistActivity.this, current_wordlist.getId(), EditWordsActivity.FROM_EDIT_WL);
            }
        });

        edit_wordlist_btn_import_from_file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                import_file();
            }
        });
    }

    // followed guide on:
    // https://developer.android.com/guide/topics/providers/document-provider.html#java
    public void import_file() {
        Intent search_file_intent = new Intent(Intent.ACTION_GET_CONTENT);
        search_file_intent.addCategory(Intent.CATEGORY_OPENABLE);
        search_file_intent.setType("text/plain");
        startActivityForResult(search_file_intent, FILE_REQ_CODE);
    }

    @Override
    public void onActivityResult(int req_code, int res_code, Intent result_data) {
        if (req_code == FILE_REQ_CODE && res_code == Activity.RESULT_OK) {
            Uri uri;
            if (result_data != null) {
                uri = result_data.getData();
                Log.i(LOG_NAME, "Uri: " + uri.toString());
                List<String> lines;
                try {
                    lines = read_lines_from_uri(uri);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                try {
                    import_raw_word_list(lines);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private List<String> read_lines_from_uri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<String> res = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            res.add(line);
        }
        reader.close();
        inputStream.close();
        return res;
    }

    private void import_raw_word_list(List<String> terms) throws SQLException {
        Dao<Words, Integer> wordsDao = getHelper().getWordsDao();
        for (String term : terms) {
            Words new_word = new Words(term, current_wordlist);
            wordsDao.create(new_word);
        }
    }

    static void call_me(Context context, int wordlist_id) {
        Intent intent = new Intent(context, EditWordlistActivity.class);
        intent.putExtra("wordlist_id", wordlist_id);
        context.startActivity(intent);
    }
}
