package kowalk.oertel.buzzword_bingo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ManageWordlistsActivity extends OrmLiteBaseActivity<BingoDbHelper> {
    private final String LOG_NAME = ManageWordlistsActivity.class.getName();
    private Spinner wordlists_spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_wordlists);

        wordlists_spinner = findViewById(R.id.managewordlists_spinner);
        Button btn_new_wordlist = findViewById(R.id.managewordlists_btn_new_wordlist);
        Button btn_edit_wordlist = findViewById(R.id.managewordlists_btn_edit_wordlist);
        Button btn_ok = findViewById(R.id.managewordlists_btn_ok);

        try {
            create_default_wordlist_if_not_exists();
        } catch (SQLException e) {
            Log.e(LOG_NAME, "Could not insert default rows into wordlists DB", e);
        }
        loadWordList();

        btn_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MainActivity.call_me(ManageWordlistsActivity.this);
            }
        });

        btn_new_wordlist.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NewWordlistActivity.call_me(ManageWordlistsActivity.this);
            }
        });

        btn_edit_wordlist.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int selected_row_id = ((WordLists) (wordlists_spinner.getSelectedItem())).getId();
                EditWordlistActivity.call_me(ManageWordlistsActivity.this, selected_row_id);
            }
        });
    }

    private void create_default_wordlist_if_not_exists() throws SQLException {
        Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
        Dao<Words, Integer> wordsDao = getHelper().getWordsDao();

        WordLists wl = new WordLists("Default");
        wordListsDao.createIfNotExists(wl);

        for (int i = 0; i < 30; i++) {
            Words word = new Words("Default word #" + i);
            word.setWordlist(wl);
            wordsDao.createIfNotExists(word);
        }
    }

    private void loadWordList() {
        try {
            Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
            List<WordLists> wordLists = new ArrayList<>(wordListsDao.queryForAll());
            ArrayAdapter<WordLists> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, wordLists);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            wordlists_spinner.setAdapter(adapter);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    static void call_me(Context context) {
        Intent intent = new Intent(context, ManageWordlistsActivity.class);
        context.startActivity(intent);
    }
}
