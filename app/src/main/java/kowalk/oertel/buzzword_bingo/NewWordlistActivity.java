package kowalk.oertel.buzzword_bingo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.Dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewWordlistActivity extends OrmLiteBaseActivity<BingoDbHelper> {

    private static final int FILE_REQ_CODE = 123789;
    private final String LOG_NAME = NewWordlistActivity.class.getName();
    private EditText new_wordlist_title_input;
    private EditText new_wordlist_new_word_input;
    private WordLists current_wordlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_wordlist);

        int wordlist_id = getIntent().getIntExtra(EditWordsActivity.INTENT_ID_STRING, -100);

        new_wordlist_new_word_input = findViewById(R.id.new_wordlist_et_input_new_word);
        Button new_wordlist_confirm_title_button = findViewById(R.id.new_wordlist_btn_confirm_title);
        Button new_wordlist_edit_words_button = findViewById(R.id.new_wordlist_btn_edit_words);
        Button new_wordlist_import_words_button = findViewById(R.id.new_wordlist_btn_import_words_from_file);
        Button new_wordlist_cancel_button = findViewById(R.id.new_wordlist_btn_cancel);
        Button new_wordlist_ok_button = findViewById(R.id.new_wordlist_btn_ok);
        Button new_wordlist_add_word_button = findViewById(R.id.new_wordlist_btn_add_new_word);
        new_wordlist_title_input = findViewById(R.id.new_wordlist_et_input_wordlist_name);

        if (wordlist_id != -100) {
            try {
                Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
                current_wordlist = wordListsDao.queryForId(wordlist_id);
                new_wordlist_title_input.setText(current_wordlist.getTitle());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        new_wordlist_add_word_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_word = new_wordlist_new_word_input.getText().toString();

                if (current_wordlist == null) {
                    Toast.makeText(NewWordlistActivity.this, "Noch keine neue Liste angelegt", Toast.LENGTH_LONG).show();
                    return;
                }

                if (new_word.isEmpty()) {
                    Toast.makeText(NewWordlistActivity.this, "Wort-Input darf nicht leer sein", Toast.LENGTH_LONG).show();
                    return;
                }

                Words words = new Words(new_word, current_wordlist);

                try {
                    Dao<Words, Integer> wordsDao = getHelper().getWordsDao();
                    wordsDao.create(words);
                } catch (SQLException e) {
                    Log.e(LOG_NAME, "confirm_title_button on_click closure: ", e);
                }
            }
        });

        new_wordlist_confirm_title_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_title = new_wordlist_title_input.getText().toString();

                if (new_title.isEmpty()) {
                    Toast.makeText(NewWordlistActivity.this, "Titel darf nicht leer sein", Toast.LENGTH_LONG).show();
                    return;
                }

                try {
                    Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
                    final List<WordLists> queryResults = wordListsDao.queryBuilder().where().eq("title", new_title).query();
                    if (queryResults.isEmpty()) {
                        WordLists wordLists = new WordLists(new_title);
                        wordListsDao.create(wordLists);
                        current_wordlist = wordLists;
                        Toast.makeText(NewWordlistActivity.this, "Wortliste angelegt.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(NewWordlistActivity.this, "Diese Wortliste existiert bereits.", Toast.LENGTH_LONG).show();
                    }
                } catch (SQLException e) {
                    Log.e(LOG_NAME, "confirm_title_button on_click closure: ", e);
                }
            }
        });

        new_wordlist_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // In case the user created a new list in this activity but does not want it to stay
                // pressing Cancel rolls the wordlist back before going back to the MainActivity
                getHelper().delete_wordlist(current_wordlist);

                MainActivity.call_me(NewWordlistActivity.this);
            }
        });

        new_wordlist_ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // In case a wordlist was created from this activity, update the persisted version just in case
                if (current_wordlist != null) {
                    try {
                        Dao<WordLists, Integer> wordListsDao = getHelper().getWordListsDao();
                        // just to be sure ALL alterations are persisted.
                        wordListsDao.update(current_wordlist);
                    } catch (SQLException e) {
                        Log.e(LOG_NAME, "ok_button on_click closure: ", e);
                    }
                }

                MainActivity.call_me(NewWordlistActivity.this);
            }
        });

        new_wordlist_edit_words_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (current_wordlist == null) {
                    Toast.makeText(NewWordlistActivity.this, "Zuerst neuen Titel eingeben und bestätigen", Toast.LENGTH_SHORT).show();
                } else {
                    EditWordsActivity.call_me(NewWordlistActivity.this, current_wordlist.getId(), EditWordsActivity.FROM_NEW);
                }
            }
        });

        new_wordlist_import_words_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                import_file();
            }
        });
    }

    // followed guide on:
    // https://developer.android.com/guide/topics/providers/document-provider.html#java
    public void import_file() {
        Intent search_file_intent = new Intent(Intent.ACTION_GET_CONTENT);
        search_file_intent.addCategory(Intent.CATEGORY_OPENABLE);
        search_file_intent.setType("text/plain");
        startActivityForResult(search_file_intent, FILE_REQ_CODE);
    }

    @Override
    public void onActivityResult(int req_code, int res_code, Intent result_data) {
        if (req_code == FILE_REQ_CODE && res_code == Activity.RESULT_OK) {
            Uri uri;
            if (result_data != null) {
                uri = result_data.getData();
                Log.i(LOG_NAME, "Uri: " + uri.toString());
                List<String> lines;
                try {
                    lines = read_lines_from_uri(uri);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                try {
                    import_raw_word_list(lines);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private List<String> read_lines_from_uri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<String> res = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            res.add(line);
        }
        reader.close();
        inputStream.close();
        return res;
    }

    private void import_raw_word_list(List<String> terms) throws SQLException {
        Dao<Words, Integer> wordsDao = getHelper().getWordsDao();
        for (String term : terms) {
            Words new_word = new Words(term, current_wordlist);
            wordsDao.create(new_word);
        }
    }

    static void call_me(Context context) {
        Intent intent = new Intent(context, NewWordlistActivity.class);
        context.startActivity(intent);
    }

    static void call_me(Context context, int wordlist_id) {
        Intent intent = new Intent(context, EditWordsActivity.class);
        intent.putExtra(EditWordsActivity.INTENT_ID_STRING, wordlist_id);
        context.startActivity(intent);
    }
}
